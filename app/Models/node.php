<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class node extends Model
{
    use HasFactory;

    protected $table = "node";
    protected $id = "id";
    protected $primaryKey = "id";
    protected $fillable = ["id", "name",'parent_id'];

    public function ChildNodes()
    {
    	return $this->hasMany(self::class, 'parent_id', 'id');
    }

   	public function Parent(){
	   	return $this->belongsTo(self::class,'parent_id')->with('Parent');
	}

	public function childrens()
    {
        return $this->hasMany(self::class, 'parent_id', 'id')->with('childrens');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\node;
use Exception;
class nodeController extends Controller
{
    
    
    public function parents($id)
    {
    	try {
    			$nodes = node::where('id', '=', $id) 
		    	->with('Parent')
		    	->get();

    	} catch (Exception $e) {
    		return response()->json(['response'=>404, 'Error Code'=> $e, 'message' => 'Nodo No encontrado']);
    	}
    
 
    	 return response()->json(['response'=>200, 'herarchy'=> $nodes, 'message' => $id]);
    }

    public function children($id)
    {
    	try {
    		$nodes = node::where('id', '=', $id) 
	    	->with('childrens')
			->get();

    	} catch (Exception $e) {
    		return response()->json(['response'=>404, 'Error Code'=> $e, 'message' => 'Nodo No encontrado']);
    	}
    	
    	return response()->json(['response'=>200, 'herarchy'=> $nodes, 'message' => $id]);
    }
}
